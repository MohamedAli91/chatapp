import { AccountDto } from "../../models/Account";
import { MessageDto } from "../../models/Message";
import { AccountService } from "../../services/account.service";
import { MessageService } from "../../services/message.service";
import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Cookie } from "ng2-cookies";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

 contacts: AccountDto[] = [];
 messages: MessageDto[] = []; 
 myId: number ;
 contactId :number = 0 ; 
 currentContact : string ="Click on a contact to start chat with .." ; 

 constructor(private accountService: AccountService ,private messageService :MessageService ,private router: Router) { }

  ngOnInit() {
    
   this.myId = +(Cookie.get("id")); 
   window.setInterval(this.checkForAccounts.bind(this), 500);
   window.setInterval(this.getMessages.bind(this), 500);  
   

  }
  
   checkForAccounts(){
    
      this.accountService.getAccounts(this.myId).then( _res => {
              console.log( _res );
        
              if ( _res ) {
//                  debugger;
                  if ( _res.status)  {
                      this.contacts = _res.data;
                    
                  }
              }

          } );

   }
  
  getMessagesOfContact(index:number){
    this.currentContact = this.contacts[index].username;
    this.contactId = this.contacts[index].id;
    
  }
  
  getMessages(){
     this.messageService.getMessages(this.myId,this.contactId).then( _res => {
              console.log( _res );
        
              if ( _res ) {
//                  debugger;
                  if ( _res.status)  {
                      this.messages = _res.data;
                    
                  }
              }

          } );
  }
  
  
  
  sendMessage(msg:string){
     console.log(msg);
    let newMessage: MessageDto = new MessageDto(); 
    newMessage.body =msg ;
    newMessage.sender_id = this.myId;
    newMessage.receiver_id = this.contactId;
    
     this.messageService.addMessage( newMessage ).then( _res => {
            if ( _res ) {
                console.log( _res );
                debugger;
            }

        } );
    
  }
  
  logOut(){
      Cookie.set("isLogged","No");
      this.router.navigate(['','login']); 
  }

}
