import { AccountDto } from "../../models/Account";
import { AccountService } from "../../services/account.service";
import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  errorMessage : string = "";
  
  constructor(private router: Router , private accountService : AccountService) { }

  ngOnInit() {
     if(Cookie.get("isLogged")=="Yes"){
            this.router.navigate(['','main']);
        }else{
            this.router.navigate(['','login']); 
        }
  }

  //login
  
  login(username:string ,password:string){
    this.errorMessage = "";
    let account:AccountDto = new AccountDto(); 
    account.password = password;
    account.username = username;
   debugger;
    if(username.replace(/\s/g, "").length!=0||password.replace(/\s/g, "").length!=0){
     this.accountService.login( account ).then( _res => {
            
            if ( _res ) {
                console.log( _res );
              if(_res.status){
                 
                 Cookie.set("isLogged","Yes");
                 Cookie.set("id",""+_res.data);
                 this.router.navigate(['', 'main']);
                
              }else{
                this.errorMessage = 'Worng username or password!'
              }
                debugger;
            }

        } );
    }else{
      this.errorMessage = 'please fill the empty fields.';
    }
  }
  
  
  //go to register page
  goToRegister(){
      this.router.navigate(['', 'register']);
  }
  
  
}
