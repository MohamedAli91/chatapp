import { AccountDto } from "../../models/Account";
import { AccountService } from "../../services/account.service";
import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Cookie } from "ng2-cookies";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  errorMessage :string ="" ;
  constructor(private router: Router , private accountService :AccountService) { }

  ngOnInit() {
     if(Cookie.get("isLogged")=="Yes"){
            this.router.navigate(['','main']);
        }
  }

  signUp(username:string,password:string){
     this.errorMessage = "";
    let account:AccountDto = new AccountDto(); 
    account.password = password;
    account.username = username;
    
    if(username.replace(/\s/g, "").length!=0||password.replace(/\s/g, "").length!=0){
       this.accountService.signUp( account ).then( _res => {
            
            if ( _res ) {
                console.log( _res );
              if(_res.status){
                 
                 Cookie.set("isLogged","Yes");
                 Cookie.set("id",""+_res.data);
                 this.router.navigate(['', 'main']);
                
              }else{
                this.errorMessage = 'This username already registered before.';
              }
                debugger;
            }

        } );
    }else{
      this.errorMessage = 'please fill the empty fields.';
    }
    
    
    
  }
  
  
  goToSignIn(){
     this.router.navigate(['', 'login']);
  }
  
  
  
}
