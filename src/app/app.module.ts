
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { MainComponent } from './components/main/main.component';

// services

import { AccountService } from './services/account.service';
import { ConfigService } from './services/config.service';
import { HttpModule } from '@angular/http';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';


const appRoutes: Routes = [{ path: 'main', component: MainComponent},
{path : 'register', component: RegisterComponent},
{path : 'login' , component : LoginComponent}];

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    RegisterComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot( appRoutes, { 'useHash': true } )
  ],
  providers: [AccountService, ConfigService],
  bootstrap: [AppComponent]
})
export class AppModule { }
