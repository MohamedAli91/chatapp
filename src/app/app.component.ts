import { Component } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { Cookie } from 'ng2-cookies/ng2-cookies';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

    constructor(  private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {

        if(Cookie.get("isLogged")=="Yes"){
            this.router.navigate(['','main']);
        }else{
            this.router.navigate(['','login']); 
        }

    }
}
