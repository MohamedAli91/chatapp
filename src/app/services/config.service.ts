import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  private restURL: string = "http://localhost:250/";
  
  constructor() { }
  
   getRestURL():string {
    return this.restURL;
  }
  
  
}
