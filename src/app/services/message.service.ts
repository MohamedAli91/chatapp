import { MessageDto } from "../models/Message";
import { NetworkResponse } from "../models/NetworkResponse";
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
@Injectable({
  providedIn: 'root'
})
export class MessageService {
  
  private restURL =  "http://localhost:250/";
  private messagesURL = 'message/all/';
  private sendURL = 'message'
  
  private headers = new Headers( { 'Content-Type': 'application/json',
  'Access-Control-Allow-Origin': '*'} );

  
  
  constructor(private http :Http) { }
  
  
  //get messages between user and specific user 
  
  getMessages(user1:number ,user2:number){
    
    let url = "";
    url = this.restURL + this.messagesURL + "?user1="+user1 +"&user2=" +user2;
 
  return this.http.get(url)
            .toPromise()
            .then( response => response.json() as NetworkResponse )
            .catch( function( error ) {
                console.log( error )
                return null;
            } );
    
  }
  
  
  
  //send message to someone
  
   addMessage( newMessage: MessageDto ): Promise<NetworkResponse> {
        let url = "";
            url = this.restURL + this.sendURL;
     debugger;
        let self = this;
        return this.http.post( url, JSON.stringify( newMessage ), { headers: this.headers } )
            .toPromise()
            .then( response => response.json() as NetworkResponse )
            .catch( function( error ) {
                console.log( error )
                return null;
            } );
    }
  
}
