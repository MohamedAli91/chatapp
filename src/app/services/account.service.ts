import { AccountDto } from "../models/Account";
import { NetworkResponse } from '../models/NetworkResponse';
import { ConfigService } from './config.service';
import { Injectable } from '@angular/core';


import { tap } from 'rxjs/operators';
import { Headers, Http } from '@angular/http';


@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private restURL =  "http://localhost:250/";
  private accountsURL = 'account/all/';
  private loginURL = 'account/login';
  private registerURL = 'account';
  
  private headers = new Headers( { 'Content-Type': 'application/json',
  'Access-Control-Allow-Origin': '*'} );
  
  constructor(private http: Http , private config: ConfigService) { }
  
  // to get all accounts on the system
  
  getAccounts(id:number):  Promise<NetworkResponse> {
    let url = "";
    url = this.restURL + this.accountsURL + id;
 
  return this.http.get(url)
            .toPromise()
            .then( response => response.json() as NetworkResponse )
            .catch( function( error ) {
                console.log( error )
                return null;
            } );
}
  
  
  // login 
  
  login(account:AccountDto){
    
       let url = "";
            url = this.restURL + this.loginURL;
     debugger;
        let self = this;
        return this.http.post( url, JSON.stringify( account ), { headers: this.headers } )
            .toPromise()
            .then( response => response.json() as NetworkResponse )
            .catch( function( error ) {
                console.log( error )
                return null;
            } );
    
  }
  
  
  // sign up 
  
  signUp(account:AccountDto){
    
     let url = "";
            url = this.restURL + this.registerURL;
     debugger;
        let self = this;
        return this.http.post( url, JSON.stringify( account ), { headers: this.headers } )
            .toPromise()
            .then( response => response.json() as NetworkResponse )
            .catch( function( error ) {
                console.log( error )
                return null;
            } );
    
    
  }
  
  
}
